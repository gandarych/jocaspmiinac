#!/bin/bash

## 03_02_systemctl_start_tomcat on ubuntu
## Reload the Systemd File
sudo systemctl daemon-reload
## Restart Tomcat
sudo systemctl start tomcat
## Note: If the Tomcat service fails to start use "journalctl -xn" as a way to know the exact errors that are occurring.
## Verify Tomcat is Running by the status
sudo systemctl status tomcat
## Output:
####   * tomcat.service - Tomcat 9 servlet container
####     Loaded: loaded (/etc/systemd/system/tomcat.service; disabled; vendor preset: enabled)
####     Active: active (running) since Sun RRRR-MM-DD 14:36:58 CET; 13ms ago
####     Process: 14390 ExecStart=/opt/tomcat/apache-tomcat-9.0.33/bin/startup.sh (code=exited, status=0/SUCCESS)
####    Main PID: 14400 (java)
####      Tasks: 6 (limit: 4915)
####     CGroup: /system.slice/tomcat.service
####             └─14400 /usr/lib/jvm/java-1.8.0-openjdk-amd64//bin/java -Djava.util.logging.config.file=/opt/tomcat/apache-tomcat-9.0.33/conf/logging.properties 
####  
####  gandalf1 systemd[1]: Starting Tomcat 9 servlet container...
####  gandalf1 startup.sh[14390]: Tomcat started.
####  gandalf1 systemd[1]: Started Tomcat 9 servlet container.
####  
## If correctly installed you’ll also be able to see the Tomcat default page by visiting http://Host_IP:8080 in your browser,
## replacing Host_IP with your server’s IP or hostname, followed by Tomcat’s port number 8080.
q

